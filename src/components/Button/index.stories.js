import React from 'react';
import Button from './index';

export default { title: 'Button' };

export const small = () => <Button size='small'>Small</Button>;

export const medium = () => <Button size='medium'>Medium</Button>;

export const large = () => <Button size='large'>Large</Button>;