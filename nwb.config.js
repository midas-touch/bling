module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'BlingUI',
      externals: {
        react: 'React'
      }
    }
  }
}
