# bling-ui

[![Travis][build-badge]][build]
[![npm package][npm-badge]][npm]
[![Coveralls][coveralls-badge]][coveralls]

A UI Component library for react created by Midas Touch.

[build-badge]: https://img.shields.io/travis/user/repo/master.png?style=flat-square
[build]: https://travis-ci.org/user/repo

[npm-badge]: https://img.shields.io/npm/v/npm-package.png?style=flat-square
[npm]: https://www.npmjs.org/package/npm-package

[coveralls-badge]: https://coveralls.io/repos/gitlab/midas-touch/bling/badge.svg?branch=master
[coveralls]: https://coveralls.io/gitlab/midas-touch/bling


